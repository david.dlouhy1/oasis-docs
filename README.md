# Oasis Docs

Repozitář pro dokumentaci nástorje Oasis

Dokumentace se sestaví pomocí:

```bash
sphinx-build -b html source .\build\html\ -a
```

Ostatní infromace jsou k dispozici právě v této dokumentaci

## Update CI image

```bash
# Configure
OASIS_DOCS_IMAGE_NAME=registry.gitlab.com/david.dlouhy1/oasis-docs
OASIS_DOCS_IMAGE_VERSION=0.0.2

# Build
docker build -t $OASIS_DOCS_IMAGE_NAME:$OASIS_DOCS_IMAGE_VERSION -f ./docker/ci/Dockerfile .

# Push
docker push $OASIS_DOCS_IMAGE_NAME:$OASIS_DOCS_IMAGE_VERSION
```
