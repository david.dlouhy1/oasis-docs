*******
Contact
*******

Author
------

- name: David Dlouhý
- e-mail: david.dlouhy@tul.cz

Supervisor
----------

- name: Ing. Lenka Kosková Třísková, Ph.D.
- contact: `Web FM TUL <https://www.fm.tul.cz/personal/lenka.koskova.triskova>`_
