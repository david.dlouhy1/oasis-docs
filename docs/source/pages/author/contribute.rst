**********
Contribute
**********

The Oasis is an open-source project distributed under the MIT License, welcoming contributions and extensions from anyone interested. Feel free to reach out via the :doc:`Contact section <../author/contact>`.
