Declare yours needs
===================

The tool operates on the principle of YAML file declaration. This way, the user defines the desired state. For better organization, there are several types of files. It is important that all files are at the same level as the ``oasis.yml`` file. It is possible to have additional files in subdirectories. The tool will recursively access them.

Oasis
-----

This type of file can have only one occurrence. You must not have two or more of these files. The purpose of the file is to provide metadata about the infrastructure. Therefore, ``name`` and ``description``. 

.. code-block:: yaml

    ---
    # oasis.yml
    oasis:
        name: "Example Oasis"
        description: "This is an example of the infrastructure defined by the Oasis tool."


Technologies
------------

Technology definitions can be split across multiple files. The tool merges them into one.

An item in the list is a technology with the following properties:

- ``name``

    - Technology Name
    - Serves as a reference and unique identifier for the technology
    - String of characters
    - Possible values: ``GitLab``, ``Jira``, ``Hetzner``, ``Prometheus``,  ``VMware``,  ``GitHub``,  ``BitBucket``

.. warning:: VMware, GitHub and BitBucket technologies are currently in experimental development and cannot be fully utilized!

- ``type``

    - Technology Type
    - Serves as an identifier for the technology type
    - String of characters
    - Possible values: ``vcs``, ``cicd``, ``plan``, ``infra``, ``monitor``

- ``definition``
    - Technology Definition
    - Used for configuring technology, such as tokens and others
    - Keys
    - Possible keys: ``url``, ``mail``, ``token``

.. code-block:: yaml

    ---
    # technologies.yml
    technologies:

      - name: <TECHNOLOGY_NAME>
        type:
         - <TECHNOLOGY_TYPE>
        definition:
          <TECHNOLOGY_DEFINITION_KEY>: <TECHNOLOGY_DEFINITION_VALUE>

.. danger:: It is possible to replace variables with plaintext data, but this is not recommended. There is a risk of leaking sensitive information!

Projects
--------

Project definitions can be split across multiple files. The tool merges them into one.

An item in the list is a project with the following properties:

- ``name``

    - Project Name
    - Serves as a reference and name for repositories
    - String of characters

- ``type``

    - Project Type
    - Serves as an identifier for the project type
    - String of characters
    - Possible values: ``Embedded OS (Linux)``, ``Golang``, ``Rust``, ``infra``, ``monitor``

.. warning:: Projects ``Golang`` and ``Rust``  are currently in experimental development and cannot be fully utilized!

- ``vcs``
    - Reference to the name of the VCS (Version Control System) technology
    - Used for assigning the VCS technology
    - String of characters

- ``infra``
    - Reference to the name of the infrastructure technology
    - Used for assigning the infrastructure technology
    - String of characters

- ``plan``
    - Reference to the name of the planning technology
    - Used for assigning the planning technology
    - String of characters

- ``monitor``
    - Reference to the name of the monitoring technology
    - Used for assigning the monitoring technology
    - String of characters

.. code-block:: yaml

    ---
    # project.yml
    projects:
      - name: <PROJECT_NAME>
        type: <PROJECT_TYPE>
        vcs: <VCS_TECHNOLOGY_NAME>
        infra: <INFRA_TECHNOLOGY_NAME>
        plan: <PLAN_TECHNOLOGY_NAME>
        monitor: <MONITOR_TECHNOLOGY_NAME>
