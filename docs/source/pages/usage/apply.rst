Apply declaration
=================

If you have a prepared declaration and want to use it, enter the following command:

.. code-block:: bash

    oasis apply --file=<FILE>

Replace the string ``<FILE>`` with the path to the file ``oasis.yml``, which contains the definition of Oasis.