Requirements
============

In this section, you will learn about the basic assumptions and requirements for your knowledge, hardware, and software.

Knowledge
---------

Basic IT knowledge in software development and management areas is assumed. Knowledge of DevOps and related technologies is also recommended.

HW
--

Your hardware must have the following specifications:

- CPU architecture must be x86/64
- at least 1 GB of free disk space
- average network connection
- at least 200 MB of free RAM"

SW
--

You must have an operating system from the following list. The latest version of the OS is recommended, but the tool supports older versions.

- Windows
- Ubuntu
- Fedora
- Debian
- RHEL
- openSUSE
- Arch
- Manjaro
- Gentoo
- Rocky Linux
- Kali
- and similar