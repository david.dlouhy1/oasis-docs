*******
Details
*******

For comprehensive insights into the implementation details, refer to my master's thesis. The thesis encapsulates a detailed exploration of the Golang hexagonal application, covering every facet from needs analysis to design and from the concrete implementation process to its execution. It serves as a comprehensive resource, offering a deep dive into the intricacies of implementing the hexagonal architecture in Golang.

Go to `official website <http://dlouhydp.tkkzmj.cz/>`_.

