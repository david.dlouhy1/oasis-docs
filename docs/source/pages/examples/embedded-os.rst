Embedded OS project
===================

This example will show you how to set up a demonstration infrastructure for developing embedded OS. The technologies used are GitLab, Hetzner, Jira, Prometheus, and Yocto Project.

1. Download
-----------

First, download the tool from the official website. You can use the graphical interface or the commands provided below. Choose the latest version according to your OS. You can also utilize a Docker container.


.. code-block:: bash
    
    # Downloading the tool for Linux
    wget https://oasis.rodina-dlouha.cz/api/files/oasis-1-0-0
    mv oasis-1-0-0 oasis
    chmod 755 oasis
    mkdir data
    mkdir declaration

    # Downloading the tool for Windows
    wget https://oasis.rodina-dlouha.cz/api/files/oasis-1-0-0
    mv oasis-1-0-0 oasis
    mkdir data
    mkdir declaration

    # Pull Docker image
    docker pull registry.gitlab.com/david.dlouhy1/oasis/oasis:1.0.0


2. Declare
----------

Create a directory named ``declaration`` and inside it, create configuration files named ``declaration/oasis.yml``, ``declaration/technologies.yml`` and ``declaration/project.yml``.

File ``declaration/oasis.yml``:

.. code-block:: yaml

    ---
    oasis:
        name: "Example Embedded Oasis"
        description: "This is an example of the infrastructure defined by the Oasis tool for embedded OS development."

.. danger:: It is possible to replace variables with plaintext data, but this is not recommended. There is a risk of leaking sensitive information!

File ``declaration/technologies.yml``:

.. code-block:: yaml

    ---
    technologies:

      - name: GitLab
        type:
         - vcs
         - cicd
        definition:
          token: $OASIS_GITLAB_TOKEN

      - name: Jira
        type:
         - plan
        definition:
          url: $OASIS_JIRA_URL
          mail: $OASIS_JIRA_MAIL
          token: $OASIS_JIRA_TOKEN

      - name: Hetzner
        type:
          - infra
        definition:
          token: $OASIS_HETZNER_TOKEN

      - name: Prometheus
        type:
          - monitor

File ``declaration/project.yml``:

.. code-block:: yaml

    ---
    projects:
      - name: "Oasis example Embedded OS project"
        type: "Embedded OS (Linux)"
        vcs: GitLab
        infra: Hetzner
        plan: Jira
        monitor: Prometheus


1. Set up system
----------------

Further, it is necessary to set the system variables used in the declaration. Set the following variables:

.. note:: I recommend defining the environment with a ``.env`` file, simply insert the variable definitions into it, and then before running Oasis, load them into the system using ``export $(xargs <.env)``

- ``$OASIS_GITLAB_TOKEN``

  - GitLab token, granting Oasis access to the GitLab cloud
  - The token must have all permissions
  - `Instructions for generating it <https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html>`_ 

- ``$OASIS_JIRA_URL``

  - Jira URL
  - If you're using the cloud version, enter ``https://<NAMESPACE>.atlassian.net/``
  - ``<NAMESPACE>`` replace it with your Jira namespace

- ``$OASIS_JIRA_MAIL``

  - Your email account used in Jira

- ``$OASIS_JIRA_TOKEN``

  - Jira token, granting Oasis access to the Jira cloud
  - The token must have all permissions
  - `Instructions for generating it <https://confluence.atlassian.com/enterprise/using-personal-access-tokens-1026032365.html>`_ 

- ``$OASIS_HETZNER_TOKEN``

  - Hetzner token, granting Oasis access to the Hetzner cloud
  - The token must have all permissions
  - `Instructions for generating it <https://docs.hetzner.com/cloud/api/getting-started/generating-api-token/>`_ 

4. Run the Oasis tool
---------------------

Now that everything is set up, simply enter the following command:

.. code-block:: bash

  ./oasis apply --file=declaration/oasis.yml

5. Familiarize yourself with your infrastructure
------------------------------------------------

All information can be found in the variables in the repository of the new project. You can connect to the server using an SSH key and find out what such a key looks like. You'll find out where the monitoring is and more. More information about variables is `in the official GitLab documentation <https://docs.gitlab.com/ee/ci/variables/>`_.

6. Begin developing the project
-------------------------------

Make changes to the project as needed. You can use the development  ``.devcontinaer``,mply open the project in VS Code and choose the option for the development container. See `official documentation <https://code.visualstudio.com/docs/devcontainers/containers>`_ for more details.

.. warning:: To use ``.devcontainer``, you must have Docker installed!

.. image:: ../../images/DP_3_18.png
  :width: 100%
  :alt:  Infrastructure diagram