.. Oasis Docs documentation master file, created by
   sphinx-quickstart on Wed Apr  14 09:54:03 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Oasis Docs
==========
Welcome to the Oasis Documentation!

This documentation is tailored for developers who are eager to leverage Oasis for configuring DevOps infrastructures. Packed with practical insights and guidelines, this documentation serves as your compass in navigating the intricacies of Oasis. Whether you're a seasoned developer or just starting your DevOps journey, you'll find a wealth of useful information here to streamline your development process and enhance your infrastructure setup.

Let's embark on this practical journey together, empowering you to harness the full potential of Oasis for your DevOps endeavors.

.. toctree::
   :maxdepth: 2
   :caption: Intro:
   
   pages/intro/intro

.. toctree::
   :maxdepth: 2
   :caption: How it works?
   
   pages/implementation/intro

.. toctree::
   :maxdepth: 2
   :caption: How to use?
   
   pages/usage/intro
   pages/usage/requirements
   pages/usage/declaration
   pages/usage/apply

.. toctree::
   :maxdepth: 2
   :caption: Examples:
   
   pages/examples/intro
   pages/examples/embedded-os

.. toctree::
   :maxdepth: 2
   :caption: Author:
   
   pages/author/contact
   pages/author/contribute